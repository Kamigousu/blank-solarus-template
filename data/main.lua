--Main Lua Script of the Quest.
--See the Lua API! http://www.solarus-games.org/doc/latest
--Thank you, Christopho + Co.!

require("scripts/features")
require("scripts/multi_events")

--This function is called when Solarus starts.
function sol.main:on_started()
  local game = game_manager:create("save_1.dat")
  game:start()
end