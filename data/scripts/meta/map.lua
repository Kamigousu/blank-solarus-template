local map_meta = sol.main.get_metatable("map")

function map_meta:get_wave()
  local game = self:get_game()

  return game:get_value("wave")
end

function map_meta:set_wave(wave)
  local game = self:get_game()

  return game:set_value("wave", wave)
end

function map_meta:get_next_wave()
  local game = self:get_game()

  local current_wave = self:get_wave()
  local next_wave = current_wave + 1
  return next_wave
end