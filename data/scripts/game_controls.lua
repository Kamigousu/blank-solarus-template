local game_meta = sol.main.get_metatable("game")

function game_meta:set_controls()
  local game = self
  
    print("Control Binding for UP: ")
    --print(game:get_command_keyboard_binding("up"))
  --game:set_command_keyboard_binding("up", w)
  --game:set_command_keyboard_binding("left", a)
  --game:set_command_keyboard_binding("down", s)
  --game:set_command_keyboard_binding("right", d)

end

function game_meta:on_key_pressed(key, modifiers)
  local hero = self:get_hero()
  --local stamina = self:get_stamina()
  --local max_stamina = self:get_max_stamina()

  if key == "left shift" then
    hero:set_walking_speed(132)
    self:set_value("shift", true)
    --if stamina <= 0 then
      --hero:set_walking_speed(88)
      --self:set_value("shift", false)
    --end
    print(key)
  end

  if key == "space" then
    local effect = self:get_command_effect("action")
    local state = hero:get_state()
    --local stamina = self:get_stamina()
    --make sure the conditions are right to dash and we're not doing something else or don't have the item that allows this
    if effect == nil and state == "free" and self:is_suspended() == false then
      local dir = hero:get_direction()
      --convert the direction we just got into radians so straight movement can use it
      if dir == 1 then dir = (math.pi/2) elseif dir == 2 then dir = math.pi elseif dir == 3 then dir = (3*math.pi/2) end
      print(hero:get_state())
      --self:remove_stamina(55)
      self:set_value("rolling", true)
      local m = sol.movement.create("straight")
      m:set_angle(dir)
      m:set_speed(325)
      m:set_max_distance(75) --you may want to make this into a variable so you could upgrade the dash
      m:set_smooth(true)
      hero:freeze()
--      hero:set_blinking(true, 200) --this is just a stylistic choice. It makes it move obvious that you can dash through enemies if you enable it, but in my situation the dashing animation didn't read as clearly.
      hero:get_sprite():set_animation("rolling", function() hero:get_sprite():set_animation("walking") end)
      sol.audio.play_sound("swim") --this is a placeholder sound effect that everyone should have, you'd want to change it probably
      m:start(hero, function() hero:unfreeze() end)
      hero:set_invincible(true, 300) --you may want to experiment with this number, which is how long the hero is invincible for
      function m:on_obstacle_reached()
        hero:unfreeze()
      end
    end
  end
end

function game_meta:on_key_released(key, moddifiers)
  local hero = self:get_hero()
  if key == "left shift" then
    hero:set_walking_speed(88)
    self:set_value("shift", false)
  end
  
  if key == "space" then    
    self:set_value("rolling", false)
  end
end
return game_meta